### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ f82fc2dd-54a6-4e3f-8518-ab1c8183f3bf
using Pkg

# ╔═╡ dcec0c82-7ccc-4ce7-81c8-d7699df671f6
Pkg.activate("Project.toml")

# ╔═╡ 88458131-c8c3-4821-9cc1-26f9d8a2aa36
Pkg.add("POMDPs"); Pkg.add("QMDP")

# ╔═╡ 9c804857-8434-4d55-8496-fb997d05dd7e
Pkg.add("InstantiateFromURL");using InstantiateFromURL

# ╔═╡ eb15bd80-1438-4c02-b1b0-466aeabc28b5
Pkg.add("Distributions");using Distributions

# ╔═╡ 4f9d70bc-fcdb-40ba-a7c2-e8546fbba33e
Pkg.add("QuantEcon");using QuantEcon

# ╔═╡ 55c75dbc-747b-4ecc-899a-6713e17d002f
Pkg.add("QuickPOMDPs"); using QuickPOMDPs

# ╔═╡ b07abad4-01cd-4c66-ad4b-0fb852010654
Pkg.add("POMDPSimulators");using POMDPSimulators;Pkg.add("POMDPModelTools");using POMDPModelTools

# ╔═╡ cf701c65-ce36-4999-8aca-433fd5e9ae79
using PlutoUI

# ╔═╡ dbf3c4e1-4d14-44b0-b5e2-8e1a9f8a1a4f
using LinearAlgebra

# ╔═╡ 1f3bbd5a-4a35-44b5-a5c4-063f08d40f4e
using Statistics

# ╔═╡ d66dfe52-2ce6-4b99-a1c4-76b28fdeee8d
 using Plots

# ╔═╡ c9b8c1bc-c1d6-4ae3-b571-11c03bc0776f
using Printf;

# ╔═╡ 55893f5a-9b19-4829-8b2a-68ab5e6c9035
using Random 

# ╔═╡ 885838eb-7f9b-4f99-8082-1bea9d4628c1
using POMDPs, QMDP

# ╔═╡ e5eca630-da5c-11eb-022d-4d4467116949
md"# Assignment 3"

# ╔═╡ 7842b1f2-6e2a-481c-bd22-919067c13db8
md"## Problem 1"

# ╔═╡ 72dfc2d4-117b-49f1-8008-4e672e42c728
instantiate = true; precompile = true 

# ╔═╡ 7f5166ac-ad0e-4182-bb44-a7c73077262c
gr(fmt = :png);

# ╔═╡ fef74480-05c2-4441-8bd2-103c118929c9
begin 
	X = Categorical([0.5, 0.3, 0.2])
	@show rand(X, 5)
	@show supertype(typeof(X))
	@show pdf(X, 1) 
	@show support(X)
	@show pdf.(X, support(X)); 
end 

# ╔═╡ c0ad7f86-eaa7-47d9-817b-1cebc4eb1ebe
begin
	L = [0.4 0.6; 0.2 0.8];
	mc = MarkovChain(L)
	W = simulate(mc, 100_000);
	μ_2 = count(W .== 1)/length(W) 
end

# ╔═╡ f87689d6-bd84-40f8-9519-7e345e8be7fb
begin
	MarkovChain(L, ["", ""])

	simulate(mc, 4, init = 1) 
end

# ╔═╡ 99819bf4-a38d-42fd-851b-a782129c9a17
simulate(mc, 4, init = 2) 

# ╔═╡ b6db3611-e770-4cf9-a13e-c135b84ad790
simulate(mc, 4) 

# ╔═╡ a8eb7581-5b6e-4550-ba84-5b1217fb6ef5
simulate_indices(mc, 4)

# ╔═╡ f5f1990e-e007-4a8a-9a29-8397f4595450
md"#### States"

# ╔═╡ 4f5a1e75-e402-46e6-b2e1-c7cad3d5b1d7
begin
one = [:left, :right]           
two = [:left, :right, :listen]  
three = [:left, :right]
four = 0.95

function starter(s, a, sp)
    if a == :listen
        return s == sp
    else # 
        return 0.5 
    end
end

function placing(a, sp, o)
    if a == :listen
        if o == sp
            return 0.85
        else
            return 0.15
        end
    else
        return 0.5
    end
end

function processing(s, a)
    if a == :listen  
        return -1.0
    elseif s == a
        return -100.0
    else 
        return 10.0
    end
end

marv = DiscreteExplicitPOMDP(one,two,three,starter,placing,processing,four)
end

# ╔═╡ 097a638f-cc0e-4d4d-bb15-7f5385e98acc
begin
	struct capture <: POMDP{Bool, Bool, Bool}
    start_car::Float64
    empty_petrol::Float64
    slowdown_car::Float64
    stop_car::Float64
    restart_car::Float64
    discount::Float64   
	end

capture() = capture(-5., -10., 0.1, 0.8, 0.1, 0.9);
end

# ╔═╡ d389cb93-2ac9-4df4-83c6-ef05c01daa95
begin
Pkg.add("POMDPPolicies");
using POMDPPolicies

markov = capture()

# policy that maps every input to a feed (true) action
policy = FunctionPolicy(o->true)

for (empty_tank, refuel, r) in stepthrough(markov, policy, "s,a,r", max_steps=10)
    @show empty_tank
    @show refuel
    @show r
    println()
end
end

# ╔═╡ 90d5f03c-9afd-42d9-80d7-9643fff72fa0
begin
	function POMDPs.gen(marvo::capture, empty_tank, refuel, rng)
    # transition model
    if refuel # refuel
        sp = false
    elseif empty_tank # empty tank
        sp = true
		else # full tank
        sp = rand(rng) < marvo.slowdown_car
    end
    
    # observation model
    if sp # empty
        o = rand(rng) < marvo.stop_car
		else # full tank
        o = rand(rng) < marvo.restart_car
    end
    
    # reward model
    r = empty_tank*marvo.empty_petrol + refuel*marvo.start_car
    
    # create and return a NamedTuple
    return (sp=sp, o=o, r=r)
end
end

# ╔═╡ 09d0cb29-2c54-4935-b7dc-ad3dfff7e167

POMDPs.initialstate_distribution(m::capture) = Deterministic(false)

# ╔═╡ Cell order:
# ╠═e5eca630-da5c-11eb-022d-4d4467116949
# ╠═7842b1f2-6e2a-481c-bd22-919067c13db8
# ╠═f82fc2dd-54a6-4e3f-8518-ab1c8183f3bf
# ╠═dcec0c82-7ccc-4ce7-81c8-d7699df671f6
# ╠═cf701c65-ce36-4999-8aca-433fd5e9ae79
# ╠═88458131-c8c3-4821-9cc1-26f9d8a2aa36
# ╠═9c804857-8434-4d55-8496-fb997d05dd7e
# ╠═72dfc2d4-117b-49f1-8008-4e672e42c728
# ╠═dbf3c4e1-4d14-44b0-b5e2-8e1a9f8a1a4f
# ╠═1f3bbd5a-4a35-44b5-a5c4-063f08d40f4e
# ╠═eb15bd80-1438-4c02-b1b0-466aeabc28b5
# ╠═d66dfe52-2ce6-4b99-a1c4-76b28fdeee8d
# ╠═c9b8c1bc-c1d6-4ae3-b571-11c03bc0776f
# ╠═4f9d70bc-fcdb-40ba-a7c2-e8546fbba33e
# ╠═55893f5a-9b19-4829-8b2a-68ab5e6c9035
# ╠═7f5166ac-ad0e-4182-bb44-a7c73077262c
# ╠═fef74480-05c2-4441-8bd2-103c118929c9
# ╠═c0ad7f86-eaa7-47d9-817b-1cebc4eb1ebe
# ╠═f87689d6-bd84-40f8-9519-7e345e8be7fb
# ╠═99819bf4-a38d-42fd-851b-a782129c9a17
# ╠═b6db3611-e770-4cf9-a13e-c135b84ad790
# ╠═a8eb7581-5b6e-4550-ba84-5b1217fb6ef5
# ╠═885838eb-7f9b-4f99-8082-1bea9d4628c1
# ╠═55c75dbc-747b-4ecc-899a-6713e17d002f
# ╠═b07abad4-01cd-4c66-ad4b-0fb852010654
# ╠═f5f1990e-e007-4a8a-9a29-8397f4595450
# ╠═4f5a1e75-e402-46e6-b2e1-c7cad3d5b1d7
# ╠═097a638f-cc0e-4d4d-bb15-7f5385e98acc
# ╠═90d5f03c-9afd-42d9-80d7-9643fff72fa0
# ╠═09d0cb29-2c54-4935-b7dc-ad3dfff7e167
# ╠═d389cb93-2ac9-4df4-83c6-ef05c01daa95
