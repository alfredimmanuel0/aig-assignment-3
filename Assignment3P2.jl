### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 105d72ca-8cae-4321-8c94-2c1511cb30b5
using Pkg

# ╔═╡ b87eda9e-3897-4b88-abe5-8702e37e7c0a
Pkg.activate("Project.toml")

# ╔═╡ 50736bab-e684-4d59-a132-024be00371f5
Pkg.add("GLM");using GLM

# ╔═╡ 9bc4ece5-6ef6-4875-85b8-ca189ed75432
using PlutoUI

# ╔═╡ da6adbaf-34e1-41c8-8cc3-c127279bf0cf
using DataFrames, Statistics, Plots

# ╔═╡ 298ac1b2-dd00-11eb-14ff-336881623d19
md"# ASSIGMNENT 3"

# ╔═╡ 160b6a06-4c91-4a5e-a2fb-4e0f862c3199
md"## Problem 2"

# ╔═╡ d66ffbc2-a116-4229-b685-b8e60e7b9739
begin
	function game(No_players, overtake=true)
    gamers = first(Int, No_players)
    while true
        for (gamers, starting_line) in enumerate(gamers)
            career = switch(gamers, starting_line, overtake)
            if career == 100
                overtake && println("Gamers $gamers wins!")
                return gamers
            end
            gamers[gamer] = career
            overtake && println()
        end
    end
end
end

# ╔═╡ 8ab02b2c-ea6b-4ac6-b4b6-d00b8d8fad1b
begin
	function seqGame(No_players, No_games)
   gamers = Beginner(Int, No_players)
    for i in 1:No_games
        champ = game(No_players, false)
        gamers[champ] += 1
    end
    println("\n\nseqGame: out of $No_games games, champs are:\n    #  |  wins \n--")
    for (i, gamers) in enumerate(gamers)
        println("    $i   $gamers")
    end
    strategy = DataFrame(A = serve(1:No_players), Z = gamers)
    fullgame = lm(@formula(Z ~ A), strategy)
    println("\nStatistics:\n", fullgame)
end
 
seqGame(2, 10)
end

# ╔═╡ Cell order:
# ╠═298ac1b2-dd00-11eb-14ff-336881623d19
# ╠═160b6a06-4c91-4a5e-a2fb-4e0f862c3199
# ╠═105d72ca-8cae-4321-8c94-2c1511cb30b5
# ╠═b87eda9e-3897-4b88-abe5-8702e37e7c0a
# ╠═9bc4ece5-6ef6-4875-85b8-ca189ed75432
# ╠═da6adbaf-34e1-41c8-8cc3-c127279bf0cf
# ╠═50736bab-e684-4d59-a132-024be00371f5
# ╠═d66ffbc2-a116-4229-b685-b8e60e7b9739
# ╠═8ab02b2c-ea6b-4ac6-b4b6-d00b8d8fad1b
